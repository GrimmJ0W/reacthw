import React, {useContext, useEffect, useState} from "react";
import {Box, Button, makeStyles, TextField} from "@material-ui/core";
import {AppContext} from "../App";


const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: theme.spacing(50),
        height: theme.spacing(8),
        border: "1px solid black",
        borderRadius: theme.spacing(1),
        margin: theme.spacing(10),
        background: "rgba(255, 255, 255)"
    },
    main: {
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        zIndex: 1000,
        background: "rgba(0, 0, 0, 0.5)"
    },
    disable: {
        display: "none"
    }
}))

const Edit = () => {
    const classes = useStyles();
    const {edit, setEdit, input, setInput} = useContext(AppContext);
    const [newValue, setNewValue] = useState({});

    useEffect(()=>{
        setNewValue({...edit})
    }, [edit])

    const handleEdit =()=>{
       if(newValue.text === '' || !input.every((e)=>e.text!==newValue.text)){
           alert("You use existing name or empty name for to Do")
       }else{
           const edited = input.map((e)=>{
               if(e.id===newValue.id){
                   return {...e, text: newValue.text}
               }else{
                   return e;
               }
           })
           setInput(edited);
           setEdit(false);
       }
    }

    return (
        <Box className={edit ? classes.main : classes.disable}>
            <Box className={classes.container}>
                <TextField onChange={(e)=>setNewValue({...newValue, text: e.target.value})} value={newValue.text}/>
                <Button onClick={handleEdit} variant="contained" color="primary">Edit</Button>
            </Box>
        </Box>
    );
}

export default Edit;
