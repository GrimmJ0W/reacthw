import React, {useContext, useEffect, useState} from "react";
import {Box, makeStyles, Typography} from "@material-ui/core";
import {AppContext} from "../App";

const useStyles = makeStyles((theme) => ({
    container: {
        width: theme.spacing(50),
        height: "auto",
        border: "1px solid black",
        borderRadius: theme.spacing(1),
        margin: "80px 80px 80px 0",
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(2)
    },
    toDo: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "80%"
    },
    main: {
        position: "absolute",
        left: theme.spacing(10),
        top: theme.spacing(50),
        padding: theme.spacing(2)

    }
}))

const DeletedToDo = () => {
    const classes = useStyles();
    const {state} = useContext(AppContext);
    const [list, setList] = useState([])

    const deletedList = list.map((e)=>{
       return(
           <Box key={Math.random()} className={classes.toDo}>
               <Typography>{e}</Typography>
           </Box>
       );
    })

    useEffect(()=>{
        setList(state.toDoList);
    },[state])

    return (
        <Box className={classes.main}>
            <Typography>Deleted toDoList</Typography>
            <Box className={classes.container}>
                {deletedList}
            </Box>
        </Box>

    );
}

export default DeletedToDo;
