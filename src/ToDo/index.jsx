import React, {useContext, useEffect, useState} from 'react';
import {Box, makeStyles, Typography, Divider, Checkbox} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import {AppContext} from "../App";

const useStyles = makeStyles((theme) => ({
    container: {
        width: theme.spacing(50),
        height: "auto",
        border: "1px solid black",
        borderRadius: theme.spacing(1),
        margin: "80px 80px 80px 0",
        display: "flex",
        flexDirection: "column"
    },
    toDo: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "80%"
    },
    done:{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "80%",
        textDecoration: "line-through",
        opacity: "0.5"
    },
    icons:{
        cursor: "pointer"
    }
}));

const ToDo = () => {
    const classes = useStyles();
    const {input, setInput, handleDelete, setEdit} = useContext(AppContext);
    const [toDo, setToDo] = useState(input);

    useEffect(()=>{
        setToDo(input);
    },[input])

    const handleChange = (e) => {
        let result = toDo.map((element) => {
            if (element.text === e.text) {
                return {...element, done: !e.done}
            }
            return element;
        });
        setInput(result);
    };

    const toDoList = toDo.filter((e) => e.done === false).map((e) => {
        return (
            <Box key={e.text} className={classes.toDo}>
                <Checkbox
                    checked={e.done}
                    onChange={() => handleChange(e)}
                    color="primary"
                    inputProps={{'aria-label': 'primary checkbox'}}
                />
                <Typography>{e.text}</Typography>
                <EditIcon onClick={()=>setEdit({...e})} className={classes.icons}/>
                <DeleteForeverIcon className={classes.icons} onClick={()=>handleDelete(e)}/>
            </Box>
        );
    })

    const doneList = toDo.filter((e) => e.done === true).map((e) => {
        return (
            <Box key={e.text} className={classes.done}>
                <Checkbox
                    checked={e.done}
                    onChange={() => handleChange(e)}
                    color="primary"
                    inputProps={{'aria-label': 'primary checkbox'}}
                />
                <Typography>{e.text}</Typography>
                <EditIcon onClick={()=>setEdit({...e})} className={classes.icons}/>
                <DeleteForeverIcon className={classes.icons} onClick={()=>handleDelete(e)}/>
            </Box>
        );
    })

    return (
        <Box className={classes.container}>
            <Typography style={{margin: "15px"}}> To do list</Typography>
            <Box>
                {toDoList}
            </Box>
            <Divider style={{margin: "15px"}} variant="middle"/>
            <Typography style={{margin: "15px"}}>Done</Typography>
            <Box>
                {doneList}
            </Box>
        </Box>
    );
}

export default ToDo;
