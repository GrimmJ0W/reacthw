import React, {createContext, useCallback, useReducer, useState, useMemo, useRef, useEffect} from "react";
import {Box, Button, Typography} from "@material-ui/core";
import Input from "./Input";
import ToDo from "./ToDo";
import Edit from "./Edit";
import "./style.css";
import DeletedToDo from "./DeletedToDo";

const initialState = {
    toDoList: []
};


export const SHOW_EDIT = "SHOW_EDIT";

const reducer=(state, action)=>{
    switch (action.type){
        case SHOW_EDIT:
            console.log(state, action.payload)
            let result = [...state.toDoList];
            result.push(action.payload)
            return {...state, toDoList: [...result]}
        default:
            return state;
    }
}


export const AppContext = createContext([]);

function App() {
    const [input, setInput] = useState([]);
    const [edit, setEdit] = useState('');
    const [state, dispatch] = useReducer(reducer, initialState);

    const appStartAt = useRef(new Date());
    const time = new Date();

    const useMyOwnHook = ()=>{
        const [render, setRender] = useState("Seconds with useRef:" + appStartAt.current.getSeconds());
        return [render,setRender];
    }

    const [showDifference] = useMyOwnHook()

    const handleChange = (toDo) =>{
        const element = {
            text: toDo,
            done: false,
            id: Math.random() + Math.random()
        }

        if(input.length===0){
            if(element.text!==''){
                setInput([{...element}]);
            }
        }else{
            if(element.text!==''){
                const finalResult =[...input];
                if(input.every((e)=>e.text!==element.text)){
                    finalResult.push(element);
                    setInput(finalResult);

                }
            }
        }
    }

    const toDoCount = useMemo(()=>{
        return input.length;
    }, [input])



    const handleDelete = useCallback((element)=>{
        const edited = input.filter((e)=>e.text!==element.text);
        setInput(edited);
        dispatch({type: SHOW_EDIT, payload: element.text});
    },[input])

    // const handleDelete = (element)=>{
    //     const edited = input.filter((e)=>e.text!==element.text);
    //     setInput(edited);
    //     dispatch({type: SHOW_EDIT, payload: element.text});
    // }


    return (
        <AppContext.Provider value={{setInput, handleChange, input, handleDelete, edit, setEdit, state}}>
            <Box display="flex">
                <Typography style={{margin: "80px 0 0 80px"}} variant="h5">To do count: {toDoCount}</Typography>
                <Typography style={{margin: "80px 10px 0 80px"}} variant="h5">App start at: {appStartAt.current.getHours() +":"+ appStartAt.current.getMinutes()+":"+ appStartAt.current.getSeconds()}</Typography>
                <Box display="flex" alignItems="flex-end">
                    <Button onClick={()=>alert(showDifference  + "   Current seconds:" + time.getSeconds())} variant="contained" color="primary">Date check</Button>
                </Box>
            </Box>
            <Box display="flex">
                <Input/>
                <ToDo />
                <Edit />
                <DeletedToDo />
            </Box>
        </AppContext.Provider>

    );
}

export default App;
