import {createMuiTheme} from "@material-ui/core";

const theme = createMuiTheme({
    palette: {
        common: {
            black: "black",
            white: "white",
            backGround: "#f6f6f6"
        },
    }
})

export default theme;
