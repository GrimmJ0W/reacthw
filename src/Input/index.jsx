import React, {useContext, useState} from "react";
import {TextField, Box, Button, makeStyles} from "@material-ui/core";
import {AppContext} from "../App";

const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: theme.spacing(50),
        height: theme.spacing(8),
        border: "1px solid black",
        borderRadius: theme.spacing(1),
        margin: theme.spacing(10)
    }
}))

const Input = () => {
    const classes = useStyles();
    const {handleChange} = useContext(AppContext);
    const [input, setInput] = useState('');

    const handleSubmit = () => {
        handleChange(input);
        setInput('')
    }

    return (
        <Box className={classes.container}>
            <TextField onChange={(e) => setInput(e.target.value)} value={input}/>
            <Button onClick={handleSubmit} variant="contained" color="primary">Submit</Button>
        </Box>
    );
}

export default Input;
